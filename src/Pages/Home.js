import React, { ReactElement, useState, useEffect } from 'react';
import axios from 'axios';
import DayWeather from '../Components/DayWeather';

const { REACT_APP_API_URL, REACT_APP_API_KEY } = process.env;

export default function Home({ locations }: Props): ReactElement {

    const [locationData, setLocationData] = useState([])
    const fetchData = async () => {
        const apiCall1 = axios.get(`${REACT_APP_API_URL}/weather?lat=49.125801&lon=8.597680&appid=${REACT_APP_API_KEY}&units=metric&lang=de`);
        const apiCall2 = axios.get(`${REACT_APP_API_URL}/weather?lat=50.110924&lon=8.682127&appid=${REACT_APP_API_KEY}&units=metric&lang=de`);
        const apiCall3 = axios.get(`${REACT_APP_API_URL}/weather?lat=52.520008&lon=13.404954&appid=${REACT_APP_API_KEY}&units=metric&lang=de`);
        const apiCall4 = axios.get(`${REACT_APP_API_URL}/weather?lat=53.551086&lon=9.993682&appid=${REACT_APP_API_KEY}&units=metric&lang=de`);
        const apiCall5 = axios.get(`${REACT_APP_API_URL}/weather?lat=48.135124&lon=11.581981&appid=${REACT_APP_API_KEY}&units=metric&lang=de`);
        axios.all([apiCall1, apiCall2, apiCall3, apiCall4, apiCall5]).then(
            axios.spread((...allData) => {
                setLocationData(allData)
            })
        )
    }
    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div className='container'>
            {locationData.map((weather, i) => (
                <DayWeather key={i} weather={weather.data} name={weather.data.name} showForeCastLink={true} />
            ))}
        </div>
    )
}