import React, { ReactElement, useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import Select from 'react-select';
import DayWeather from '../Components/DayWeather';

const { REACT_APP_API_URL, REACT_APP_API_KEY } = process.env;

export default function SDForecast({ locations }: Props): ReactElement {
    let navigate = useNavigate();
    let { location } = useParams();
    const [weatherData, setWeatherData] = useState([])
    let currentLocation = locations.find(o => o.label === location);

    useEffect(() => {
        if (location != undefined) {
            let currentLocation = locations.find(o => o.label === location);
            axios.get(`${REACT_APP_API_URL}/onecall?exclude=current&${currentLocation.value}&appid=${REACT_APP_API_KEY}&units=metric`).then(res => {
                setWeatherData(res.data.daily)
            })
        }
    }, [location]);

    const onSelect = async (e) => {
        let cancelToken;
        const search = e.value;
        let currentLocation = locations.find(o => o.value === search);
        if (typeof cancelToken != typeof undefined) {
            cancelToken.cancel('canceling the previews request!')
        }
        cancelToken = axios.CancelToken.source()
        axios.get(`${REACT_APP_API_URL}/onecall?exclude=current&${search}&appid=${REACT_APP_API_KEY}&units=metric`,
            { cancelToken: cancelToken.token })
        navigate('/forecast/' + currentLocation.label)
    }

    return (
        <div className='container'>
            <h3 className="mb-4 mt-4 pb-2 fw-normal">Check {location} 7 Days weather forecast</h3>
            <div className="container">
                <div className="row">
                    <div className="col"></div>
                    <div className="col"></div>
                    <div className="col align-self-end">
                        <Select options={locations} onChange={onSelect} value={currentLocation} />
                    </div>
                </div>
            </div>
            {weatherData.map((weather, i) => (
                <DayWeather key={i} weather={weather} name={location} />
            ))}
        </div>
    )
}
