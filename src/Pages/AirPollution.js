import React, { ReactElement, useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import Select from 'react-select';

const { REACT_APP_API_URL, REACT_APP_API_KEY } = process.env;

export default function AirPollution({ locations }: Props): ReactElement {
    let navigate = useNavigate();
    let { location } = useParams();
    const qualityIndex = [
        { value: '1', label: 'Good' },
        { value: '2', label: 'Fair' },
        { value: '3', label: 'Moderate' },
        { value: '4', label: 'Poor' },
        { value: '5', label: 'Very Poor' }
    ]
    const [weatherData, setWeatherData] = useState([])

    useEffect(() => {
        if (location != undefined) {
            let currentLocation = locations.find(o => o.label === location);
            axios.get(`${REACT_APP_API_URL}/air_pollution?${currentLocation.value}&appid=${REACT_APP_API_KEY}&units=metric`).then(res => {
                setWeatherData([res.data.list[0]])
            })
        }
    }, [location]);

    const onSelect = (e) => {
        let cancelToken;
        const search = e.value;
        let currentLocation = locations.find(o => o.value === search);
        if (typeof cancelToken != typeof undefined) {
            cancelToken.cancel('canceling the previews request!')
        }
        cancelToken = axios.CancelToken.source()
        axios.get(`${REACT_APP_API_URL}/air_pollution?${search}&appid=${REACT_APP_API_KEY}&units=metric`,
            { cancelToken: cancelToken.token })
        //window.location = window.location.origin + '/airpollution/' + currentLocation.label;
        navigate( '/airpollution/' + currentLocation.label)
    }
    let currentLocation = locations.find(o => o.label === location);

    return (
        <div className='container'>
            <h3 className="mb-4 mt-4 pb-2 fw-normal">Check {location} Air Pulution</h3>
            <div className="container">
                <div className="row">
                    <div className="col"></div>
                    <div className="col"></div>
                    <div className="col align-self-end">
                        <Select options={locations} onChange={onSelect} value={currentLocation} />
                    </div>
                </div>
            </div>
            {weatherData.length > 0 ?
                <table className="table table-bordered mt-4">
                    <tbody>
                        <tr>
                            <th>CO</th>
                            <td>{weatherData[0].components.co} μg/m3</td>
                        </tr>
                        <tr>
                            <th>nh3</th>
                            <td>{weatherData[0].components.nh3} μg/m3</td>
                        </tr>
                        <tr>
                            <th>no</th>
                            <td>{weatherData[0].components.no} μg/m3</td>
                        </tr>
                        <tr>
                            <th>no2</th>
                            <td>{weatherData[0].components.no2} μg/m3</td>
                        </tr>
                        <tr>
                            <th>o3</th>
                            <td>{weatherData[0].components.o3} μg/m3</td>
                        </tr>
                        <tr>
                            <th>pm2_5</th>
                            <td>{weatherData[0].components.pm2_5} μg/m3</td>
                        </tr>
                        <tr>
                            <th>pm10</th>
                            <td>{weatherData[0].components.pm10} μg/m3</td>
                        </tr>
                        <tr>
                            <th>so2</th>
                            <td>{weatherData[0].components.so2} μg/m3</td>
                        </tr>
                        <tr>
                            <th>Air Quality Index</th>
                            <td>{qualityIndex.find(o => o.value == weatherData[0].main.aqi).label}</td>
                        </tr>
                    </tbody>
                </table>
                : ''
            }
        </div>
    )
}