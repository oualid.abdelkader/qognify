import React, { ReactElement } from 'react'
import Moment from 'react-moment'
import { Link } from 'react-router-dom';


export default function Weather({ weather, name, showForeCastLink }: Props): ReactElement {
    return (
        <section>
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-md-8 col-lg-6 col-xl-4"><div className="card shadow-0 border">
                        <div className="card-body p-4">
                            <p className="mb-2 text-end"><strong><Moment format="D MMM YYYY" unix>{weather.dt}</Moment></strong></p>
                            <h4 className="mb-1 sfw-normal">{name}</h4>
                            <p className="mb-2">Weather description: <strong>{weather.weather[0].description}</strong></p>
                            <p className="mb-2">Humidity: <strong>{weather.humidity ? weather.humidity : weather.main.humidity}%</strong></p>
                            <p className="mb-2">Current temperature: <strong>{weather.temp ? weather.temp.day : weather.main.temp}°C</strong></p>
                            <p>Max: <strong>{weather.temp ? weather.temp.max : weather.main.temp_max}°C</strong>, Min: <strong>{weather.temp ? weather.temp.min : weather.main.temp_min}°C</strong></p>
                            <div className="d-flex flex-row align-items-center">
                                <p className="mb-2">
                                    <img src={"http://openweathermap.org/img/wn/" + weather.weather[0].icon + "@2x.png"} /></p>
                            </div>
                            {showForeCastLink ? <Link to={"/forecast/" + name} className='align-self-end'>
                                7 Days Weather Forecast
                            </Link> : ''}
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

//aktuelle Wetter-Beschreibung. 
//Feuchtigkeit, aktuelle Temperatur, minimale und maximale Temperatur.