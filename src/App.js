import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Home from './Pages/Home';
import SDForecast from './Pages/SDForecast';
import AirPollution from './Pages/AirPollution';
import ErrorPage from './Pages/ErrorPage';
import { Navbar, Nav, Container } from 'react-bootstrap';

function App() {
  const locations = [
    { value: 'lat=49.125801&lon=8.597680', label: 'Bruchsal' },
    { value: 'lat=50.110924&lon=8.682127', label: 'Frankfurt am Main' },
    { value: 'lat=52.520008&lon=13.404954', label: 'Berlin' },
    { value: 'lat=53.551086&lon=9.993682', label: 'Hamburg' },
    { value: 'lat=48.135124&lon=11.581981', label: 'München' }
  ]
  return (
    <Router>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">OpenWeather</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/forecast">Forecast</Nav.Link>
            <Nav.Link as={Link} to="/airpollution">AirPollution</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <Routes>
        <Route path='/' element={<Home locations={locations} />} />
        <Route path='/forecast' element={<SDForecast locations={locations} />} />
        <Route path='/forecast/:location' element={<SDForecast locations={locations} />} />
        <Route path='/airpollution' element={<AirPollution locations={locations} />} />
        <Route path='/airpollution/:location' element={<AirPollution locations={locations} />} />
        <Route path='*' element={<ErrorPage />} />
      </Routes>
    </Router>
  );
}

export default App;
